﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Catalog.Contracts.Repositories
{
    public interface IRepository<TEntity>
     where TEntity : class
    {
        void Commit();
        void Delete(object id);
        void Delete(TEntity entity);
        void Dispose();
        System.Linq.IQueryable<TEntity> GetAll();
        TEntity GetById(object id);
        void Insert(TEntity entity);
        void Update(TEntity entity);
    }
}
