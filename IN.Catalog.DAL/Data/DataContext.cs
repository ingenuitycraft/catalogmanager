﻿using IN.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Catalog.DAL.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("DefaultConnection")
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>().HasOptional(c => c.Parent).WithMany(c => c.Children).HasForeignKey(c => c.ParentId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Category>().HasMany<Product>(c => c.Products).WithOptional().WillCascadeOnDelete(true);
        }
    }
}
