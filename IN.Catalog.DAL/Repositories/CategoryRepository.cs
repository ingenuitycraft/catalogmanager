﻿using IN.Catalog.DAL.Data;
using IN.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Catalog.DAL.Repositories
{
    public class CategoryRepository : Repository<Category>
    {
        public CategoryRepository(DataContext context)
            : base(context)
        {
        }
    }
}
