﻿using IN.Catalog.DAL.Data;
using IN.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Catalog.DAL.Repositories
{
    public class ProductRepository : Repository<Product>
    {
        public ProductRepository(DataContext context)
            : base(context)
        {

        }
    }
}
