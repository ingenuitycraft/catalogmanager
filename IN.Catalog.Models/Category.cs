﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Catalog.Models
{
    public class Category
    {
        //public Category ParentCategory { get; set; }

        public int CategoryId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        public string CategoryName { get; set; }

        public ICollection<Category> Children { get; set; }

        public ICollection<Product> Products { get; set; }

        public int? ParentId { get; set; }

        public Category Parent { get; set; }

        [MaxLength(255)]
        public string ImageUrl { get; set; }
    }
}
