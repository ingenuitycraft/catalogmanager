﻿using IN.Catalog.Contracts.Repositories;
using IN.Catalog.Models;
using IN.Catalog.WebServ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IN.Catalog.WebServ.Controllers
{
    public class CategoryController : Controller
    {
        IRepository<Product> products;
        IRepository<Category> categories;

        public CategoryController(IRepository<Product> products, IRepository<Category> categories)
        {
            this.products = products;
            this.categories = categories;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            var model = categories.GetAll().ToList();
            return View(model);
        }

        public ActionResult Create(int? id, int? pid)
        {
            var model = new CategoryViewModel();

            var all = categories.GetAll().ToList();

            model.Categories = all;
            model.Category = new Category();

            if (pid.HasValue)
            {
                model.Category.Parent = all.FirstOrDefault(c => c.CategoryId == pid);

                if (model.Category.Parent != null)
                {
                    model.Category.ParentId = model.Category.Parent.CategoryId;
                }
            }

            model.UrlTemplate = "/Category/Create/{0}/{1}";

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Category category)
        {
            categories.Insert(category);
            categories.Commit();

            return RedirectToAction("List");
        }

        public ActionResult Edit(int id, int? pid)
        {
            var all = categories.GetAll().ToList();
            var category = all.FirstOrDefault(c => c.CategoryId == id);

            if (category == null)
            {
                return RedirectToAction("List");
            }

            if (pid == null)
            {
                pid = category.ParentId;
            }

            category.Parent = all.FirstOrDefault(c => c.CategoryId == pid);

            if (category.Parent == null)
            {
                category.ParentId = null;
            }
            else
            {
                category.ParentId = category.Parent.CategoryId;
            }

            all.Remove(category);

            var model = new CategoryViewModel();

            model.Categories = all;
            model.Category = category;
            model.UrlTemplate = "/category/edit/{0}/{1}";

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            categories.Update(category);
            categories.Commit();

            return RedirectToAction("List");
        }

        public ActionResult Delete(int id)
        {

            var category = categories.GetById(id);

            if (category == null)
            {
                return RedirectToAction("List");
            }

            var all = categories.GetAll().ToList();

            var children = all.Where(item => item.ParentId == category.CategoryId).ToList();

            if (children != null)
            {
                DeleteChildren(children, all);
            }

            categories.Delete(category);

            categories.Commit();

            return RedirectToAction("List");
        }

        void DeleteChildren(IList<Category> children, IEnumerable<Category> all)
        {
            foreach (var child in children)
            {
                var grands = all.Where(item => item.ParentId == child.CategoryId).ToList();

                if (grands.Count > 0)
                {
                    DeleteChildren(grands, all);
                }

                categories.Delete(child);
            }
        }

        public ActionResult Details(int id)
        {
            var all = categories.GetAll().ToList();
            var category = all.FirstOrDefault(c => c.CategoryId == id);

            if (category == null)
            {
                return RedirectToAction("List");
            }

            int? pid = category.ParentId;

            category.Parent = all.FirstOrDefault(c => c.CategoryId == pid);
            category.ParentId = pid;

            all.Remove(category);

            category.Products = products.GetAll().Where(p => p.CategoryId == category.CategoryId).ToList();

            var model = new CategoryDetailsViewModel();

            model.Categories = all;
            model.Category = category;

            return View(model);
        }
    }
}