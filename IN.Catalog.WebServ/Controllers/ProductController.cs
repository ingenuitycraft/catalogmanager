﻿using IN.Catalog.Contracts.Repositories;
using IN.Catalog.Models;
using IN.Catalog.WebServ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IN.Catalog.WebServ.Controllers
{
    public class ProductController : Controller
    {
        IRepository<Product> products;
        IRepository<Category> categories;

        public ProductController(IRepository<Product> products, IRepository<Category> categories)
        {
            this.products = products;
            this.categories = categories;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            var model = new ProductListViewModel();

            model.Categories = categories.GetAll().ToList();
            model.Products = products.GetAll().OrderBy(p => p.ProductName).ToList();

            return View(model);
        }

        public ActionResult Create(int? id, int? pid)
        {
            var model = new ProductViewModel();

            var all = categories.GetAll().ToList();
            var product = new Product();
            Category category = null;

            if (pid.HasValue)
            {
                category = all.FirstOrDefault(c => c.CategoryId == pid);
            }
            else
            {
                if (all.Count > 0)
                {
                    category = all.Where(c => c.ParentId == null).OrderBy(c => c.CategoryId).First();
                }
            }

            if (category != null)
            {
                product.CategoryId = category.CategoryId;
            }

            model.Categories = all;
            model.Product = product;
            model.SelectedCategoryId = pid.HasValue ? pid.Value : 0;
            model.UrlTemplate = "/product/create/{0}/{1}";

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Product product)
        {
            if (product.CategoryId == 0)
            {
                return RedirectToAction("create", new { id = product.ProductId, pid = product.CategoryId });
            }

            products.Insert(product);
            products.Commit();

            return RedirectToAction("details", "Category", new { id = product.CategoryId });
        }

        public ActionResult Edit(int id, int pid)
        {
            var product = products.GetById(id);

            if (product == null)
            {
                return RedirectToAction("List");
            }

            var all = categories.GetAll().ToList();
            var model = new ProductViewModel();

            model.Categories = all;
            model.Product = product;
            model.SelectedCategoryId = pid;
            model.UrlTemplate = "/product/edit/{0}/{1}";

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            if (product.CategoryId == 0)
            {
                return RedirectToAction("Edit", new { id = product.ProductId, pid = product.CategoryId });
            }

            products.Update(product);
            products.Commit();

            return RedirectToAction("Details", "Category", new { id = product.CategoryId });
        }

        public ActionResult Delete(int id)
        {
            var product = products.GetById(id);
            var categoryId = product.CategoryId;

            products.Delete(id);
            products.Commit();

            return RedirectToAction("Details", "Category", new { id = categoryId });
        }

    }
}