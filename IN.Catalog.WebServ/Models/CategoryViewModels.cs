﻿using IN.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IN.Catalog.WebServ.Models
{
    public class CategoryViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public Category Category { get; set; }
        public string UrlTemplate { get; set; }
    }

    public class CategoryDetailsViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public Category Category { get; set; }
    }
}