﻿using IN.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IN.Catalog.WebServ.Models
{
    public class ProductViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public Product Product { get; set; }
        public int SelectedCategoryId { get; set; }
        public string UrlTemplate { get; set; }
    }

    public class ProductListViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Product> Products { get; set; }

    }
}